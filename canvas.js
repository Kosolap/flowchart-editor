
var context;

var previousSelectedCircle;

var isDragging = false;

//���������� ��� �������� ������
var curLord;
var curVassal;
var lordExist = false;

//���������� ��� �����
var circleExist = false;
var cercle;

//������� ��� �������� ��������
var recMass = [];
var brillMass = [];
var ellMass = [];
var ciclMass = [];
var connections = [];

//������� ��� ���������� ��������
var recMassBackUp = [];
var brillMassBackUp = [];
var ellMassBackUp = [];
var ciclMassBackUp = [];
var connectionsBackUp = [];

//**************************************������� �� ������ � �������************************************

window.onload = function() {
	canvas = document.getElementById("drawingCanvas");
	context = canvas.getContext("2d");
	
       context.font = "12px Verdana, sans-serif";
	   
      // ���������� ��������� ��� ��������� �������
		 canvas.onmousedown = canvasClick;   
		 canvas.onmouseup = stopDragging;
         canvas.onmouseout = stopDragging;
         canvas.onmousemove = dragCircle;
	  

   }
   
// ����������� ������� <img> ��� ������� �����, �� �������� ����� ��������
var previousThicknessElement;

function changeThickness (thickness, imgElement)
{
    // �������� ������� ������� �����
	context.lineWidth = thickness;
	
	// ������ ����� �������� <img>, �� �������� ��������
	imgElement.className = "Selected";
	
	// ���������� ����� ��������� ������� <img> � ���������� ���������
	if (previousThicknessElement != null)
	   previousThicknessElement.className = "";
	   
	previousThicknessElement = imgElement;
}


//������� ������
function clearCanvas() {
    cercle = null;
	recMass = [];
	brillMass = [];
	context.clearRect(0, 0, canvas.width, canvas.height);
	
	//������ ����������� ����������� ���������
	var imageContainer = document.getElementById("savedCopyContainer");
    imageContainer.style.visibility = 'hidden';
}

//���������� ���������
function saveCanvas() {

    // ������� ������� <img>
	var imageCopy = document.getElementById("savedImageCopy");
	
	// ���������� ������ ������ � �������� <img>
	imageCopy.src = canvas.toDataURL();
	
	// ���������� ������� <div>, ����� ����������� �������
	// ����� ����������� �������
	var imageContainer = document.getElementById("savedCopyContainer");
    imageContainer.style.display = "block";
	
	//����������� �������� ��������
	recMassBackUp = recMass;
	brillMassBackUp = brillMass;
	connectionsBackUp = connections;
	ellMassBackUp = ellMass;
	ciclMassBackUp = ciclMass;
}

//�������� ���������
function loadCanvas(){

recMass = recMassBackUp;
brillMass = brillMassBackUp;

drawAll();
}

//**************************************������� �������� � �� ���������************************************

//**************************************�������������*********************

//������ �������������

function Rectangle(x, y, height, width, id) {
    this.x = x;
    this.y = y;
    this.height = height;
	this.width = width;
	this.id = id;
    this.isSelected = false;
	this.text = "";
	
	this.startDrX = 0;
	this.startDrY = 0;
	
	//var gateL = [this.x, this.y+this.height/2];
	//var gateR = [this.x+this.width, this.y+this.height/2];
	this.gateT = [this.x+this.width/2, this.y];
	this.gateD = [this.x+this.width/2, this.y + this.height];
	
	this.gates = [0, 0, this.gateT, this.gateD];
	
	this.type = "rec";
	
	this.isConnected = false;
	this.connections = 0;
}

//�������� ��������������

function addRectangle() {
	
    // ������������� ������ � ������� ��������������
    var height = 60;
	var width = 108;
    var x = 15;
    var y = 15;

	
    // ���������� �������������
    //var color = Black;

    // ������� ����� �������������
    var rectangle = new Rectangle(x, y, height, width, recMass.length);

    // ��������� ��� � �������
    recMass.push(rectangle);

    // ��������� ����������� ��������
    drawAll();
}

//��������� ��������������

function drawRectangle() {

    // ������ ��� �������������� �� �������
	for(var i=0; i<recMass.length; i++)
	{
		context.strokeStyle = "black";
		
		if(recMass[i].isSelected == true)
		{
			context.lineWidth = 3;
			context.font = "bold 12px Verdana, sans-serif";
		}
		
		context.globalAlpha = 0.85;
        context.beginPath();
        context.strokeRect(recMass[i].x, recMass[i].y, recMass[i].width, recMass[i].height);
        
		
		if(recMass[i].text) 
		{
			text = recMass[i].text;
			var arr = [];
			var position = recMass[i].y + 14;
			for(var j=0; j<text.length/9; j++ ){
			arr.push(text.slice(9*j, 9*j+9));
			}

			for(var j = 0; j<arr.length; j++)
			{
				context.fillText(arr[j], recMass[i].x + 10, position);
				position += 10;
			}				
		}
		
		var isLord = false;
								
		for(var n = 0; n < connections.length; n++)
		{
			//�������� �������� �� ������������� ���� ���� ������
			if(connections[n].lord.id == i && connections[n].lord.type == "rec") isLord = true;
		}
		
		
		if(isLord == false && circleExist == false) 
		{
			drawHelpCircle(recMass[i].gateD[0],recMass[i].gateD[1]);

		}

        context.fill();
        context.stroke(); 
		context.lineWidth = 1;
		context.font = "12px Verdana, sans-serif";
	}
	


}

//**************************************����********************************

//������ ����
function Brill(x, y, height, width, id) {
    this.x = x;
    this.y = y;
    this.height = height;
	this.width = width;
    this.isSelected = false;
	this.text = "";
	
	this.startDrX = 0;
	this.startDrY = 0;
	
	this.gateL = [this.x, this.y];
	this.gateR = [this.x+this.width, this.y];
	this.gateT = [this.x+this.width/2, this.y - this.height/2];
	this.gateD = [this.x+this.width/2, this.y + this.height/2];
	
	this.gates = [this.gateL, this.gateR, this.gateT, this.gateD];
	
	this.type = "brill";
	
	this.isConnected = false;
	this.connections = 0;
	
	this.id = id;
}

//�������� �����

function addBrill() {
	
    // ������������� ������ � ������� ��������������
    var height = 60;
	var width = 108;
    var x = 15;
    var y = 45;

    // ���������� �������������
    //var color = Black;

    // ������� ����� �������������
    var brill = new Brill(x, y, height, width, brillMass.length);

    // ��������� ��� � �������
    brillMass.push(brill);

    // ��������� ����������� ��������
    drawAll();
}

// ��������� �����
function drawBrill(){

    // ������ ��� ����� �� �������
	for(var i=0; i<brillMass.length; i++)
	{
		context.strokeStyle = "black";
		
		var brill = brillMass[i];
		
		if(brill.isSelected == true)
		{
			context.lineWidth = 3;
		}
		
		
		context.globalAlpha = 0.85;
        context.beginPath();
		
        context.moveTo(brill.x, brill.y);
		context.lineTo((brill.x +(brill.width/2)), (brill.y + (brill.height/2)));
		
		context.moveTo((brill.x +(brill.width/2)), (brill.y + (brill.height/2)));
		context.lineTo((brill.x + brill.width), brill.y);
		
		context.moveTo((brill.x + brill.width), brill.y);
		context.lineTo((brill.x +(brill.width/2)), (brill.y - (brill.height/2)));
		
		context.moveTo((brill.x +(brill.width/2)), (brill.y - (brill.height/2)));
		context.lineTo(brill.x, brill.y);
	
		
		context.fill();
        context.stroke(); 
		context.lineWidth = 1;
		
		
		//��������� ��������������� �����
		var isLordL = false;
		var isLordR = false;
								
		for(var n = 0; n < connections.length; n++)
		{
			//�������� �������� �� ���� ���� ���� ������
			if(connections[n].lord.id == i && connections[n].lord.type == "brill" && connections[n].lord.side == "left") isLordL = true;
			
			if(connections[n].lord.id == i && connections[n].lord.type == "brill" && connections[n].lord.side == "right") isLordR = true;
		}
		
		
		if(isLordL == false && circleExist == false) drawHelpCircle(brill.gateL[0], brill.gateL[1]);
		if(isLordR == false && circleExist == false) drawHelpCircle(brill.gateR[0], brill.gateR[1]);


		
		
		if(brill.text) 
		{
			text = brill.text;
			var arr = [];
			var position = brill.y+2;
			var position2 = brill.x + 16;
			for(var j=0; j<text.length/9; j++ ){
			arr.push(text.slice(9*j, 9*j+9));
			}

			for(var j = 0; j<arr.length; j++)
			{
				context.fillStyle = "black";
				context.fillText(arr[j], position2, position);
				position += 10;
				position2 += 8;
			}				
		}
		
        context.strokeStyle = "black";
		



	}
		
}

//**************************************����********************************

//������ ����
function Ellipse(x, y, height, width, id){
	this.x = x;
	this.y = y;
	this.height = height;
	this.width = width;
	this.isSelected = false;
	this.text = "";

	this.startDrX = 0;
	this.startDrY = 0;

	this.gateT = [this.x+(this.width-this.height)/2, this.y];
	this.gateD = [this.x+(this.width-this.height)/2, this.y + this.height];
	
	this.gates = [0, 0, this.gateT, this.gateD];

	this.type = "ell";	
	
	this.isConnected = false;
	this.connections = 0;
	
	this.id = id;
	
	//��������������� ����������
	
	//����� ����� ����������
	this.centL = [this.x, this.y + this.height/2];
	this.centR = [this.x+(this.width-this.height), this.y + this.height/2];
}

//�������� �����
function addEllipse() {
	
    // ������������� ������ � ������� �����
    var height = 60;
	var width = 108;
    var x = 45;
    var y = 15;

    // ���������� �������������
    //var color = Black;

    // ������� ����� ����
    var ell = new Ellipse(x, y, height, width, ellMass.length);

    // ��������� ��� � �������
    ellMass.push(ell);

    // ��������� ����������� ��������
    drawAll();
}

// ��������� �����
function drawEllipse(){

// ������ ��� ����� �� �������
	for(var i=0; i<ellMass.length; i++)
	{
		context.strokeStyle = "black";
		
		var ell = ellMass[i];

		
		if(ell.isSelected == true)
		{
			context.lineWidth = 3;
		}
		
		
		context.globalAlpha = 0.85;
        context.beginPath();
		
        context.moveTo(ell.x, ell.y);
		//context.lineTo(ell.x +(ell.width-ell.height), ell.y);
		
		context.moveTo(ell.x +(ell.width-ell.height), ell.y+ell.height);
		context.lineTo(ell.x, ell.y+ell.height);
		
		context.arc(ell.centL[0], ell.centL[1], ell.height/2, 0.5*Math.PI, 1.5*Math.PI);
		context.arc(ell.centR[0], ell.centR[1], ell.height/2, 1.5*Math.PI, 0.5*Math.PI);
		

        context.stroke(); 

		
		if(ell.isConnected == false && circleExist == false) 
		{
			drawHelpCircle(ell.gateD[0], ell.gateD[1]);

		}
		
		if(ell.text) 
		{
			context.fillStyle = "black";
			text = ell.text;
			var arr = [];
			var position = ell.centL[1];
			for(var j=0; j<text.length/9; j++ ){
			arr.push(text.slice(9*j, 9*j+9));
			}

			for(var j = 0; j<arr.length; j++)
			{
				context.fillText(arr[j], ell.centL[0]-5, position);
				position += 10;
			}				
		}
		
        context.strokeStyle = "black";
		
		context.lineWidth = 1;

	}
		
}

//**************************************������� �����************************

//������ ������� �����
function Cicle(x, y, height, tWidth, dWidth, cType, id) {
    this.x = x;
    this.y = y;
    this.height = height;
	this.tWidth = tWidth;
	this.dWidth = dWidth;
	this.cType = cType;
	this.id = id;
    this.isSelected = false;
	this.text = "";
	
	this.startDrX = 0;
	this.startDrY = 0;
	
	if(this.cType==0)
	{
		this.gateT = [this.x+this.dWidth/2, this.y - this.height];
		this.gateD = [this.x+this.dWidth/2, this.y];
	}
	
	else{
		this.gateT = [this.x+this.tWidth/2, this.y];
		this.gateD = [this.x+this.tWidth/2, this.y + this.height];
	}

	
	this.gates = [0, 0, this.gateT, this.gateD];
	
	this.type = "cicl";
	
	this.isConnected = false;
	this.connections = 0;
}

//�������� ������� ������� �����
function addTopCicle() {

    // ������������� ������ � ������� ������� �����
    var height = 60;
	var dWidth = 108;
	var tWidth = 78;
    var x = 15;
    var y = 75;
	var cType = 0;
	
    // ���������� ������� �����
    //var color = Black;

    // ������� ����� ������� �����
    var cicle = new Cicle(x, y, height, dWidth, tWidth, cType, ciclMass.length);

    // ��������� ��� � �������
    ciclMass.push(cicle);

    // ��������� ����������� ��������
    drawAll();
}

//�������� ������ ������� �����
function addDownCicle() {
	
    // ������������� ������ � ������� ������� �����
    var height = 60;
	var dWidth = 78;
	var tWidth = 108;
    var x = 15;
    var y = 15;
	var cType = 1;
	
    // ���������� ������� �����
    //var color = Black;

    // ������� ����� ������� �����
    var cicle = new Cicle(x, y, height, dWidth, tWidth, cType, ciclMass.length);

    // ��������� ��� � �������
    ciclMass.push(cicle);

    // ��������� ����������� ��������
    drawAll();
}

// ������ ��� ������� ����� �� �������
function drawCicle(){

for(var i=0; i<ciclMass.length; i++)
	{
		context.strokeStyle = "black";
		
		var cicl = ciclMass[i];

		
		if(cicl.isSelected == true)
		{
			context.lineWidth = 3;
		}
		
		
		context.globalAlpha = 0.85;
        context.beginPath();
		
		
		//���������� ������� �������
		if(cicl.cType == 0)
		{

		context.beginPath();
		context.moveTo(cicl.x, cicl.y);
		context.lineTo(cicl.x + cicl.dWidth, cicl.y);
		
		context.moveTo(cicl.x + cicl.dWidth, cicl.y);
		context.lineTo(cicl.x + cicl.dWidth, cicl.y-(cicl.height-15));
		
		context.moveTo(cicl.x + cicl.dWidth, cicl.y-(cicl.height-15));
		context.lineTo(cicl.x + (cicl.dWidth-15), cicl.y-cicl.height);

		context.moveTo(cicl.x + (cicl.dWidth-15), cicl.y-cicl.height);
		context.lineTo(cicl.x + 15, cicl.y-cicl.height);

		context.moveTo(cicl.x + 15, cicl.y-cicl.height);
		context.lineTo(cicl.x, cicl.y-(cicl.height-15));

		context.moveTo(cicl.x, cicl.y-(cicl.height-15));
		context.lineTo(cicl.x, cicl.y);		
	
        context.stroke(); 
		}

		//���������� ������ �������		
		else{
			
		context.beginPath();
		context.moveTo(cicl.x, cicl.y);
		context.lineTo(cicl.x + cicl.tWidth, cicl.y);
		
		context.moveTo(cicl.x + cicl.tWidth, cicl.y);
		context.lineTo(cicl.x + cicl.tWidth, cicl.y+(cicl.height-15));
		
		context.moveTo(cicl.x + cicl.tWidth, cicl.y+(cicl.height-15));
		context.lineTo(cicl.x + (cicl.tWidth-15), cicl.y+cicl.height);

		context.moveTo(cicl.x + (cicl.tWidth-15), cicl.y+cicl.height);
		context.lineTo(cicl.x + 15, cicl.y+cicl.height);

		context.moveTo(cicl.x + 15, cicl.y+cicl.height);
		context.lineTo(cicl.x, cicl.y+(cicl.height-15));

		context.moveTo(cicl.x, cicl.y+(cicl.height-15));
		context.lineTo(cicl.x, cicl.y);		
	
        context.stroke(); 			
		}

		//���������� ��������������� ����� �� ������
		var isLord = false;
								
		for(var n = 0; n < connections.length; n++)
		{
			//�������� �������� �� ������������� ���� ���� ������
			if(connections[n].lord.id == i && connections[n].lord.type == "cicl") isLord = true;
		}
		
		
		if(isLord == false && circleExist == false) 
		{
			
			drawHelpCircle(ciclMass[i].gateD[0],ciclMass[i].gateD[1]);

		}
		
		//���������� ������ � �������
		if(cicl.text) 
		{
			
			if(cicl.cType == 0)
			{
				context.fillStyle = "black";
				text = cicl.text;
				var arr = [];
				var position = cicl.y - 30;
				for(var j=0; j<text.length/8; j++ )
				{
					arr.push(text.slice(8*j, 8*j+8));
				}

				for(var j = 0; j<arr.length; j++)
				{
					context.fillText(arr[j], cicl.x + 5, position);
					position += 10;
				}	
			}
			
			else{
				context.fillStyle = "black";
				text = cicl.text;
				var arr = [];
				var position = cicl.y + 12;
				for(var j=0; j<text.length/8; j++ )
				{
					arr.push(text.slice(8*j, 8*j+8));
				}

				for(var j = 0; j<arr.length; j++)
				{
					context.fillText(arr[j], cicl.x + 5, position);
					position += 10;
				}	
			}
			
		}
		
		
        context.strokeStyle = "black";
		
		context.lineWidth = 1;

	}
}		

//**************************************��������� �����**********************

//������ ����(����� �����)
function Circle(x, y, radius) {
    this.x = x;
    this.y = y;
	this.radius = radius;
    this.isSelected = false;
}

//�������� �����
function addCircle(x,y) {
	
    // ������������� ������ � ������� ��������������
    var radius = 5;

    // ������� ����� �������������
    var circle = new Circle(x, y, radius);

    // ��������� ��� � ���������� ����������
    cercle = circle;
	circleExist = true;

    // ��������� ����������� ��������
    drawAll();
}

//��������� �����
function drawCircle(){

		context.globalAlpha = 0.85;
        context.beginPath();

		context.arc(cercle.x, cercle.y, cercle.radius, 0, Math.PI*2);
		

		
		context.strokeStyle = "black";
		

        context.fill();
        context.stroke(); 
}

//��������� ��������������� �����
function drawHelpCircle(x,y){
	context.globalAlpha = 0.85;
    context.beginPath();
	context.fillStyle = "red";
	context.strokeStyle = "red";

	context.arc(x, y, 2, 0, Math.PI*2);
	
	
	context.fill();
    context.stroke(); 

}

//**************************************������� ��� ������*******************
//�������� ������� Target
function Target(type, id, side) {
this.type = type;
this.id = id;
this.side = side;
}

//�������� ������� Segment
function Segment(sX, sY, eX, eY) {
this.sX = sX;
this.sY = sY;
this.eX = eX;
this.eY = eY;
}

//������� ��������� ������� Segment
function drawSegment(segment) {

		context.globalAlpha = 0.85;
        context.beginPath();
		
        context.moveTo(segment.sX, segment.sY);
		context.lineTo(segment.eX, segment.eY);
		
		context.strokeStyle = "black";

        context.fill();
        context.stroke(); 
}

//�������� ������� Connection
function Connection(lordX, vassalX) {
this.lord = lordX;
this.vassal = vassalX;
this.segments = [];
}

//������� ��������� �������� Connection
function drawConnection() {


for(var i = 0; i < connections.length; i++)
	{

		for(var z = 0; z < connections[i].segments.length; z++)
		{
			drawSegment(connections[i].segments[z]);
		}
	}
}

//������ Point ��� �������� �����
function Point(x,y)
{
	this.x = x;
	this.y = y;
}

//**************************************������� ������ � ���������************************************

//��������� ���� ��������
function drawAll(){
	// �������� �����
    context.clearRect(0, 0, canvas.width, canvas.height);
	
	context.strokeStyle = "black";
	context.fillStyle = "black";
	
	drawRectangle();
	drawBrill();
	drawEllipse();
	drawCicle();
	
	if(circleExist)	drawCircle();
	
	drawConnection();
}


//�������� �������
function deletObj(){
	if(previousSelectedCircle != null)
	{
		
	for(var i = 0; i < connections.length; i++)
	{
		
		if(connections[i].lord.type == previousSelectedCircle.type && connections[i].lord.id == previousSelectedCircle.id || connections[i].vassal.type == previousSelectedCircle.type && connections[i].vassal.id == previousSelectedCircle.id)
		{
			  //������� ���������� � ����� �� ��������
			  var lord = connections[i].lord;
			  var vassal = connections[i].vassal;
			  
		  
			  if(lord.type == "rec")
			  {
				  recMass[lord.id].connections--;
				  if(recMass[lord.id].connections == 0) recMass[lord.id].isConnected = false;
			  }
			  
			  else if(lord.type == "brill")
			  {
  				  brillMass[lord.id].connections --;
				  if(brillMass[lord.id].connections == 0) brillMass[lord.id].isConnected = false;
			  }
			  
			  else if(lord.type == "ell")
			  {
				  ellMass[lord.id].connections--;
				  if(ellMass[lord.id].connections == 0) ellMass[lord.id].isConnected = false;
			  }
			  
			  else if(lord.type == "cicl")
			  {
				  ciclMass[lord.id].connections--;
				  if(ciclMass[lord.id].connections == 0) ciclMass[lord.id].isConnected = false;
			  }
			  
			  if(vassal.type == "rec")
			  {
				  recMass[vassal.id].connections--;
				  if(recMass[vassal.id].connections == 0) recMass[vassal.id].isConnected = false;
			  }
			  
			  else if(vassal.type == "brill")
			  {
				  brillMass[vassal.id].connections--;
				  if(brillMass[vassal.id].connections == 0) brillMass[vassal.id].isConnected = false;
			  }
			  
			  else if(vassal.type == "ell")
			  {
				  ellMass[vassal.id].connections--;
				  if(ellMass[vassal.id].connections == 0) ellMass[vassal.id].isConnected = false;
			  }
			  
			  else if(vassal.type == "cicl")
			  {
				  ciclMass[vassal.id].connections--;
				  if(ciclMass[vassal.id].connections == 0) ciclMass[vassal.id].isConnected = false;
			  }
			  
			  //������� ����������
			  connections.splice(i,1);

			i--;
		}
		

	}

		if(previousSelectedCircle.type == "rec")
		{
			for(var i = 0; i < recMass.length; i++)
			if(previousSelectedCircle.id == recMass[i].id) recMass.splice(i,1);
		}			
		else if(previousSelectedCircle.type == "brill")
		{
			for(var i = 0; i < brillMass.length; i++)
			if(previousSelectedCircle.id == brillMass[i].id) brillMass.splice(i,1);
		}	
		else if(previousSelectedCircle.type == "ell")
		{
			for(var i = 0; i < ellMass.length; i++)
			if(previousSelectedCircle.id == ellMass[i].id) ellMass.splice(i,1);
		}
		else if(previousSelectedCircle.type == "cicl")
		{
			for(var i = 0; i < ciclMass.length; i++)
			if(previousSelectedCircle.id == ciclMass[i].id) ciclMass.splice(i,1);
		}


		
	drawAll();
	}
	
	else alert("������� �������� �������!");
}
//**************************************������� �� ������� � ��������**********

// ������������ ����� �� ������
function canvasClick(e) {

  // �������� ���������� ����� ������, � ������� ��������
  var clickX = e.pageX - canvas.offsetLeft;
  var clickY = e.pageY - canvas.offsetTop;
  
  //�������� ������ �� ������� �����
  for(var i = 0; i < ciclMass.length; i++)
  {
	var cicl = ciclMass[i];
	  
	var tPikX = cicl.gateT[0];
	var tPikY = cicl.gateT[1];
	
	var dPikX = cicl.gateD[0];
	var dPikY = cicl.gateD[1];
	
			//�������� � ������� ����� ������� �����
			if(Math.abs(clickX-tPikX) <= 10 && Math.abs(clickY-tPikY) <= 10)
			{
				
				//����� �������� �������� �� ������� ����� �����
				if(lordExist == false) return;
				
				//���������� ����� � ������� ����� �����
				else{
				curVassal = new Target("cicl", i, "top");
				
				for(var j = 0; j < connections.length; j++)
				{
					if(connections[j].lord.type == curVassal.type && connections[j].lord.id == curVassal.id && connections[j].lord.side == curVassal.side) return;
					if(connections[j].vassal.type == curVassal.type && connections[j].vassal.id == curVassal.id && connections[j].vassal.side == curVassal.side) return;
				}

				//�������� � ��������� ��������, ��� � ��� ���� �����
				if(curLord.type == "brill")
				brillMass[curLord.id].isConnected = true;
			
				else if (curLord.type == "rec")
				recMass[curLord.id].isConnected = true;
			
				else if (curLord.type == "ell")
				ellMass[curLord.id].isConnected = true;
			
				else if (curLord.type == "cicl")
				ciclMass[curLord.id].isConnected = true;
				
				ciclMass[i].isConnected = true;

				//����������� ������� ������ � ��������� ��������
				if(curLord.type == "brill")
				brillMass[curLord.id].connections ++;
			
				else if (curLord.type == "rec")
				recMass[curLord.id].connections ++;

				else if (curLord.type == "ell")
				ellMass[curLord.id].connections ++;

				else if (curLord.type == "cicl")
				ciclMass[curLord.id].connections ++;				

				ciclMass[i].connections ++;
				
				lordExist = false;
				circleExist = false;
				
				makeConnection(curLord, curVassal);
				return;
					
				}
			}
  
    		//�������� � ������ ����� ������� �����
			if(Math.abs(clickX-dPikX) <= 5 && Math.abs(clickY-dPikY) <= 5)
			{
				if(lordExist == false)
				{
				curLord = new Target("cicl", i, "down");
				
				for(var j = 0; j < connections.length; j++)
				{
					if(connections[j].lord.type == curLord.type && connections[j].lord.id == curLord.id && connections[j].lord.side == curLord.side) return;
					if(connections[j].vassal.type == curLord.type && connections[j].vassal.id == curLord.id && connections[j].vassal.side == curLord.side) return;
				}
				lordExist = true;
				previousSelectedCircle.isSelected = false;
				
				addCircle(dPikX,dPikY);
				
				
				//���������� �����, � ����� ������ ������������ ����� ������� ������ ������� �����
				for(var z = 0; z<brillMass.length; z++)
				{
					//����� �� ����� � �������� ��� ��� ������
					if(brillMass[z].isConnected == false) drawHelpCircle(brillMass[z].gateT[0],brillMass[z].gateT[1]);

					//����� �� ����� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ���� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "brill") isVassal = true;
						}

						if(isVassal == false && z != i) drawHelpCircle(brillMass[z].gateT[0],brillMass[z].gateT[1]);
					}
				}
				
				//���������� �����, � ����� ��������������� ������������ ����� ������� ������ ������� �����
				for(var z = 0; z<recMass.length; z++)
				{
					//����� �� �������������� � �������� ��� ��� ������
					if(recMass[z].isConnected == false && z != i) {
						drawHelpCircle(recMass[z].gateT[0],recMass[z].gateT[1]);
					}
					
					//����� �� �������������� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ������������� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "rec") isVassal = true;
						}

						if(isVassal == false) drawHelpCircle(recMass[z].gateT[0],recMass[z].gateT[1]);
					}
				}
				
				//���������� �����, � ����� ������ ������������ ����� ������� ������ ������� �����
				for(var z = 0; z<ellMass.length; z++)
				{
					//����� �� ���� � �������� ��� ��� ������
					if(ellMass[z].isConnected == false) {
						drawHelpCircle(ellMass[z].gateT[0],ellMass[z].gateT[1]);
					}
					
				}
				
				//���������� �����, � ����� �������� ����� ������������ ����� ������� ������ ������� �����
				for(var z = 0; z<ciclMass.length; z++)
				{
					//����� �� ������� ����� � �������� ��� ��� ������
					if(ciclMass[z].isConnected == false && z != i) {
						drawHelpCircle(ciclMass[z].gateT[0],ciclMass[z].gateT[1]);
					}
					
					//����� �� ������� ����� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ������� ����� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "cicl") isVassal = true;
						}

						if(isVassal == false  && z != i) drawHelpCircle(ciclMass[z].gateT[0],ciclMass[z].gateT[1]);
					}
				}

				return;
				}
				
				else return;
			}	
  
  	// ����������, ��������� �� �����, � ������� ��������, � ������ ������� �����
	//��� ������� �������
    if(cicl.cType == 0)
	{
		//���������� ������� ������������
		var xA = cicl.x;
		var yA = cicl.y - cicl.height + 15;
		
		var xB = cicl.x + 15;
		var yB = cicl.y - cicl.height;
		
		var xC = cicl.x + 15;
		var yC = cicl.y - cicl.height + 15;
		
		var s1 = tetra(clickX, clickY, xA, yA, xB, yB);
		var s2 = tetra(clickX, clickY, xB, yB, xC, yC);
		var s3 = tetra(clickX, clickY, xC, yC, xA, yA);
		
		//��������� � ������ �����������
		var tetFir = (((s1*s2)>0) && ((s2*s3)>0));
		
		//���������� ������� ������������
		xA = cicl.x + cicl.dWidth-15;
		yA = cicl.y - cicl.height+15;
		
		xB = cicl.x + cicl.dWidth-15;
		yB = cicl.y - cicl.height;
		
		xC = cicl.x + cicl.dWidth;
		yC = cicl.y - cicl.height+15;
		
		s1 = tetra(clickX, clickY, xA, yA, xB, yB);
		s2 = tetra(clickX, clickY, xB, yB, xC, yC);
		s3 = tetra(clickX, clickY, xC, yC, xA, yA);		
		
		//��������� �� ������ �����������
		var tetSec = (((s1*s2)>0) && ((s2*s3)>0));
		
		
		if ((clickX>=cicl.x && clickX<=cicl.x + cicl.dWidth&& clickY<=cicl.y && clickY >= cicl.y-cicl.height+15) ||
		(clickX>=cicl.x+15 && clickX<=cicl.x + cicl.dWidth - 15&& clickY<= cicl.y - cicl.height + 15 && clickY >= cicl.y-cicl.height) ||
		tetFir || tetSec) 
		{
			if(circleExist == true) circleExist = false;
			if(lordExist == true) lordExist = false;
		
	
		
			// ���������� ���������� ��������� ������	
			if (previousSelectedCircle != null) previousSelectedCircle.isSelected = false;
			previousSelectedCircle = cicl;

			// ������������� ����� ��������� ������ � ��������� �����
			cicl.isSelected = true;
			drawAll();
	  
			isDragging = true;
	  
			previousSelectedCircle.startDrX = clickX - previousSelectedCircle.x;
			previousSelectedCircle.startDrY = clickY - previousSelectedCircle.y;
			// ���������� ��������
			return;
		}
	}

	
	//��� ������ �������
	else
	{
		//���������� ������� ������������
		var xA = cicl.x;
		var yA = cicl.y + cicl.height - 15;
		
		var xB = cicl.x + 15;
		var yB = cicl.y + cicl.height - 15;
		
		var xC = cicl.x + 15;
		var yC = cicl.y + cicl.height;
		
		var s1 = tetra(clickX, clickY, xA, yA, xB, yB);
		var s2 = tetra(clickX, clickY, xB, yB, xC, yC);
		var s3 = tetra(clickX, clickY, xC, yC, xA, yA);
		
		//��������� � ������ �����������
		var tetFir = (((s1*s2)>0) && ((s2*s3)>0));
		
		//���������� ������� ������������
		xA = cicl.x + cicl.tWidth-15;
		yA = cicl.y + cicl.height-15;
		
		xB = cicl.x + cicl.tWidth;
		yB = cicl.y + cicl.height-15;
		
		xC = cicl.x + cicl.dWidth - 15;
		yC = cicl.y + cicl.height;
		
		s1 = tetra(clickX, clickY, xA, yA, xB, yB);
		s2 = tetra(clickX, clickY, xB, yB, xC, yC);
		s3 = tetra(clickX, clickY, xC, yC, xA, yA);		
		
		//��������� �� ������ �����������
		var tetSec = (((s1*s2)>0) && ((s2*s3)>0));
		
		
		if ((clickX>=cicl.x && clickX<=cicl.x + cicl.tWidth&& clickY>=cicl.y && clickY <= cicl.y+cicl.height-15) ||
		(clickX>=cicl.x+15 && clickX<=cicl.x + cicl.tWidth - 15&& clickY>= cicl.y + cicl.height - 15 && clickY <= cicl.y+cicl.height) ||
		tetFir || tetSec) 
		{
			if(circleExist == true) circleExist = false;
			if(lordExist == true) lordExist = false;
		
	
		
			// ���������� ���������� ��������� ������	
			if (previousSelectedCircle != null) previousSelectedCircle.isSelected = false;
			previousSelectedCircle = cicl;

			// ������������� ����� ��������� ������ � ��������� �����
			cicl.isSelected = true;
			drawAll();
	  
			isDragging = true;
	  
			previousSelectedCircle.startDrX = clickX - previousSelectedCircle.x;
			previousSelectedCircle.startDrY = clickY - previousSelectedCircle.y;
			// ���������� ��������
			return;
		}
	}
  }
  
  //��������� �������� �� �� �����
  for(var i = 0; i < ellMass.length; i++)
  {
	var ell = ellMass[i];
	  
	var tPikX = ell.gateT[0];
	var tPikY = ell.gateT[1];
	
	var dPikX = ell.gateD[0];
	var dPikY = ell.gateD[1];
	
			//�������� � ������� ����� �����
			if(Math.abs(clickX-tPikX) <= 10 && Math.abs(clickY-tPikY) <= 10)
			{

				//����� �������� �������� �� ������� ����� �����
				if(lordExist == false) return;
				
				//���������� ����� � ������� ����� �����
				else{
				curVassal = new Target("ell", i, "top");
				
				for(var j = 0; j < connections.length; j++)
				{
					if(connections[j].lord.type == curVassal.type && connections[j].lord.id == curVassal.id && connections[j].lord.side == curVassal.side) return;
					if(connections[j].vassal.type == curVassal.type && connections[j].vassal.id == curVassal.id && connections[j].vassal.side == curVassal.side) return;
				}

				//�������� � ��������� ��������, ��� � ��� ���� �����
				if(curLord.type == "brill")
				brillMass[curLord.id].isConnected = true;
			
				else if (curLord.type == "rec")
				recMass[curLord.id].isConnected = true;
			
				else if (curLord.type == "ell")
				ellMass[curLord.id].isConnected = true;
			
				else if (curLord.type == "cicl")
				ciclMass[curLord.id].isConnected = true;
				
				ellMass[i].isConnected = true;

				//����������� ������� ������ � ��������� ��������
				if(curLord.type == "brill")
				brillMass[curLord.id].connections ++;
			
				else if (curLord.type == "rec")
				recMass[curLord.id].connections ++;

				else if (curLord.type == "ell")
				ellMass[curLord.id].connections ++;

				else if (curLord.type == "cicl")
				ciclMass[curLord.id].connections ++;			

				ellMass[i].connections ++;
				
				lordExist = false;
				circleExist = false;
				
				makeConnection(curLord, curVassal);
				return;
					
				}
			}
  
  			//�������� � ������ ����� �����
			if(Math.abs(clickX-dPikX) <= 10 && Math.abs(clickY-dPikY) <= 10)
			{
				if(lordExist == false)
				{
				curLord = new Target("ell", i, "down");
				
				for(var j = 0; j < connections.length; j++)
				{
					if(connections[j].lord.type == curLord.type && connections[j].lord.id == curLord.id && connections[j].lord.side == curLord.side) return;
					if(connections[j].vassal.type == curLord.type && connections[j].vassal.id == curLord.id && connections[j].vassal.side == curLord.side) return;
				}
				lordExist = true;
				previousSelectedCircle.isSelected = false;
				
				addCircle(dPikX,dPikY);
				
				
				//���������� �����, � ����� ������ ������������ ����� ������� ������ ����
				for(var z = 0; z<brillMass.length; z++)
				{
					//����� �� ����� � �������� ��� ��� ������
					if(brillMass[z].isConnected == false) drawHelpCircle(brillMass[z].gateT[0],brillMass[z].gateT[1]);

					//����� �� ����� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ���� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "brill") isVassal = true;
						}

						if(isVassal == false && z != i) drawHelpCircle(brillMass[z].gateT[0],brillMass[z].gateT[1]);
					}
				}
				
				//���������� �����, � ����� ��������������� ������������ ����� ������� ������ ����
				for(var z = 0; z<recMass.length; z++)
				{
					//����� �� �������������� � �������� ��� ��� ������
					if(recMass[z].isConnected == false && z != i) {
						drawHelpCircle(recMass[z].gateT[0],recMass[z].gateT[1]);
					}
					
					//����� �� �������������� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ������������� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "rec") isVassal = true;
						}

						if(isVassal == false) drawHelpCircle(recMass[z].gateT[0],recMass[z].gateT[1]);
					}
				}
				
				//���������� �����, � ����� ������ ������������ ����� ������� ������ ����
				for(var z = 0; z<ellMass.length; z++)
				{
					//����� �� ���� � �������� ��� ��� ������
					if(ellMass[z].isConnected == false && z != i) {
						drawHelpCircle(ellMass[z].gateT[0],ellMass[z].gateT[1]);
					}
					
				}
				
				//���������� �����, � ����� �������� ����� ������������ ����� ������� ������ ����
				for(var z = 0; z<ciclMass.length; z++)
				{
					//����� �� ������� ����� � �������� ��� ��� ������
					if(ciclMass[z].isConnected == false && z != i) {
						drawHelpCircle(ciclMass[z].gateT[0],ciclMass[z].gateT[1]);
					}
					
					//����� �� ������� ����� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ������� ����� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "cicl") isVassal = true;
						}

						if(isVassal == false) drawHelpCircle(ciclMass[z].gateT[0],ciclMass[z].gateT[1]);
					}
				}

				return;
				}
				
				else return;
			}	
  
  	// ����������, ��������� �� �����, � ������� ��������, � ������ �����
    if (
	(clickX>=ell.x && clickX<=ell.centR[0]&& clickY>=ell.y && clickY<=(ell.y+ell.height)) ||
	(Math.sqrt(Math.pow(ell.centL[0] - clickX, 2) + Math.pow(ell.centL[1] - clickY, 2)) <= ell.height/2) ||
	(Math.sqrt(Math.pow(ell.centR[0] - clickX, 2) + Math.pow(ell.centR[1] - clickY, 2)) <= ell.height/2)
	) 
	{
		if(circleExist == true) circleExist = false;
		if(lordExist == true) lordExist = false;
		
	  // ���������� ���������� ��������� ������	
      if (previousSelectedCircle != null) previousSelectedCircle.isSelected = false;
      previousSelectedCircle = ell;

      // ������������� ����� ��������� ������������� � ��������� �����
      ell.isSelected = true;
      drawAll();
	  
	  isDragging = true;
	  
	  previousSelectedCircle.startDrX = clickX - previousSelectedCircle.x;
	  previousSelectedCircle.startDrY = clickY - previousSelectedCircle.y;
	  // ���������� ��������
      return;
    }
  }
  
  //��������� �������� �� �� ����� ����� ��������
  for(var i = connections.length-1; i>=0; i--)
  {
	  var connection = connections[i];
	  
	  for(var j = 0; j<connection.segments.length; j++)
	  {
		  var xdiff1 = connection.segments[j].sX - clickX;
		  var ydiff1 = connection.segments[j].sY - clickY;
		  
		  var dist1 = Math.pow((xdiff1 * xdiff1 + ydiff1 * ydiff1), 0.5);
		  
		  var xdiff2 = connection.segments[j].eX - clickX;
		  var ydiff2 = connection.segments[j].eY - clickY;
		  
		  var dist2 = Math.pow((xdiff2 * xdiff2 + ydiff2 * ydiff2), 0.5);
		  
		  var xdiff3 = connection.segments[j].sX - connection.segments[j].eX;
		  var ydiff3 = connection.segments[j].sY - connection.segments[j].eY;
		  
		  var dist3 = Math.pow((xdiff3 * xdiff3 + ydiff3 * ydiff3), 0.5);
		  
		  var test = dist3 - (dist1+dist2); 
		  
		  //�������� ����� ��� ����� �� ��
		  if(test <= 0.1 && test >= -0.1)
		  {
			  //������� ���������� � ����� �� ��������
			  var lord = connections[i].lord;
			  var vassal = connections[i].vassal;
			  
		  
			  if(lord.type == "rec")
			  {
				  recMass[lord.id].connections--;
				  if(recMass[lord.id].connections == 0) recMass[lord.id].isConnected = false;
			  }
			  
			  else if(lord.type == "brill")
			  {
				  brillMass[lord.id].connections--;

				  if(brillMass[lord.id].connections == 0) brillMass[lord.id].isConnected = false;
			  }
			  
			  else if(lord.type == "ell")
			  {
				  ellMass[lord.id].connections--;
				  if(ellMass[lord.id].connections == 0) ellMass[lord.id].isConnected = false;
			  }
			  			  
			  else if(lord.type == "cicl")
			  {
				  ciclMass[lord.id].connections--;
				  if(ciclMass[lord.id].connections == 0) ciclMass[lord.id].isConnected = false;
			  }
			  
			  if(vassal.type == "rec")
			  {
				  recMass[vassal.id].connections--;
				  if(recMass[vassal.id].connections == 0) recMass[vassal.id].isConnected = false;
			  }
			  
			  else if(vassal.type == "brill")
			  {
				  brillMass[vassal.id].connections--;
				  if(brillMass[vassal.id].connections == 0) brillMass[vassal.id].isConnected = false;
			  }
			  
			  else if(vassal.type == "ell")
			  {
				  ellMass[vassal.id].connections--;
				  if(ellMass[vassal.id].connections == 0) ellMass[vassal.id].isConnected = false;
			  }
			  
			  else if(vassal.type == "cicl")
			  {
				  ciclMass[vassal.id].connections--;
				  if(ciclMass[vassal.id].connections == 0) ciclMass[vassal.id].isConnected = false;
			  }
			  
			  
			  //������� ����������
			  connections.splice(i,1);
			  drawAll();
			  return;
		  }

  	  
		  		  
	  }
  }

  // ���������, �������� �� no ��������������
  for(var i=recMass.length-1; i>=0; i--) 
  {
    var rectangle = recMass[i];
	
	var tPikX = rectangle.gateT[0];
	var tPikY = rectangle.gateT[1];
	
	var dPikX = rectangle.gateD[0];
	var dPikY = rectangle.gateD[1];
	
			//�������� � ������� ����� ��������������
			if(Math.abs(clickX-tPikX) <= 10 && Math.abs(clickY-tPikY) <= 10)
			{

				//����� �������� �������� �� ������� ����� ��������������
				if(lordExist == false) return;
				
				//���������� ����� � ������� ����� ��������������
				else{
				curVassal = new Target("rec", i, "top");
				
				for(var j = 0; j < connections.length; j++)
				{
					if(connections[j].lord.type == curVassal.type && connections[j].lord.id == curVassal.id && connections[j].lord.side == curVassal.side) return;
					if(connections[j].vassal.type == curVassal.type && connections[j].vassal.id == curVassal.id && connections[j].vassal.side == curVassal.side) return;
				}

				//�������� � ��������� ��������, ��� � ��� ���� �����
				if(curLord.type == "brill")
				brillMass[curLord.id].isConnected = true;
			
				else if (curLord.type == "rec")
				recMass[curLord.id].isConnected = true;
			
				else if (curLord.type == "ell")
				ellMass[curLord.id].isConnected = true;	

				else if (curLord.type == "cicl")
				ciclMass[curLord.id].isConnected = true;			
				
				recMass[i].isConnected = true;

				//����������� ������� ������ � ��������� ��������
				if(curLord.type == "brill")
				brillMass[curLord.id].connections ++;
			
				else if (curLord.type == "rec")
				recMass[curLord.id].connections ++;

				else if (curLord.type == "ell")
				ellMass[curLord.id].connections ++;

				else if (curLord.type == "cicl")
				ciclMass[curLord.id].connections ++;			

				recMass[i].connections ++;
				
				lordExist = false;
				circleExist = false;
				
				makeConnection(curLord, curVassal);
				return;
					
				}
			}

			//�������� � ������ ����� ��������������
			if(Math.abs(clickX-dPikX) <= 10 && Math.abs(clickY-dPikY) <= 10)
			{
				if(lordExist == false)
				{
				curLord = new Target("rec", i, "down");
				
				for(var j = 0; j < connections.length; j++)
				{
					if(connections[j].lord.type == curLord.type && connections[j].lord.id == curLord.id && connections[j].lord.side == curLord.side) return;
					if(connections[j].vassal.type == curLord.type && connections[j].vassal.id == curLord.id && connections[j].vassal.side == curLord.side) return;
				}
				lordExist = true;
				previousSelectedCircle.isSelected = false;
				
				addCircle(dPikX,dPikY);
				
				
				//���������� �����, � ����� ������ ������������ ����� ������� ������ �������������
				for(var z = 0; z<brillMass.length; z++)
				{
					//����� �� ����� � �������� ��� ��� ������
					if(brillMass[z].isConnected == false) drawHelpCircle(brillMass[z].gateT[0],brillMass[z].gateT[1]);

					//����� �� ����� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ���� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "brill") isVassal = true;
						}

						if(isVassal == false && z != i) drawHelpCircle(brillMass[z].gateT[0],brillMass[z].gateT[1]);
					}
				}
				
				//���������� �����, � ����� ��������������� ������������ ����� ������� ������ �������������
				for(var z = 0; z<recMass.length; z++)
				{
					//����� �� �������������� � �������� ��� ��� ������
					if(recMass[z].isConnected == false && z != i) {
						drawHelpCircle(recMass[z].gateT[0],recMass[z].gateT[1]);
					}
					
					//����� �� �������������� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ���� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "rec") isVassal = true;
						}

						if(isVassal == false  && z != i) drawHelpCircle(recMass[z].gateT[0],recMass[z].gateT[1]);
					}
				}
				
				//���������� �����, � ����� ������ ������������ ����� ������� ������ �������������
				for(var z = 0; z<ellMass.length; z++)
				{
					//����� �� ���� � �������� ��� ��� ������
					if(ellMass[z].isConnected == false) {
						drawHelpCircle(ellMass[z].gateT[0],ellMass[z].gateT[1]);
					}
					
				}
				
				//���������� �����, � ����� �������� ����� ������������ ����� ������� ������ ����
				for(var z = 0; z<ciclMass.length; z++)
				{
					//����� �� ������� ����� � �������� ��� ��� ������
					if(ciclMass[z].isConnected) {
						drawHelpCircle(ciclMass[z].gateT[0],ciclMass[z].gateT[1]);
					}
					
					//����� �� ������� ����� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ������� ����� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "cicl") isVassal = true;
						}

						if(isVassal == false) drawHelpCircle(ciclMass[z].gateT[0],ciclMass[z].gateT[1]);
					}
				}

				return;
				}
				
				else return;
			}			
	

	
	// ����������, ��������� �� �����, � ������� ��������, � ������ ��������������
    if (clickX>=rectangle.x && clickX<=(rectangle.x+rectangle.width)&& clickY>=rectangle.y && clickY<=(rectangle.y+rectangle.height)) 
	{
		if(circleExist == true) circleExist = false;
		if(lordExist == true) lordExist = false;
		
	
		
	  // ���������� ���������� ��������� �������������	
      if (previousSelectedCircle != null) previousSelectedCircle.isSelected = false;
      previousSelectedCircle = rectangle;

      // ������������� ����� ��������� ������������� � ��������� �����
      rectangle.isSelected = true;
      drawAll();
	  
	  isDragging = true;
	  
	  previousSelectedCircle.startDrX = clickX - previousSelectedCircle.x;
	  previousSelectedCircle.startDrY = clickY - previousSelectedCircle.y;
	  // ���������� ��������
      return;
    }
  }
  
    // ���������, �������� �� no �����
  for(var i=brillMass.length-1; i>=0; i--) 
  {
    var brill = brillMass[i];

	var cX = brill.x + (brill.width/2);
	var cY = brill.y;
	
	var lPikX = brill.gateL[0];
	var lPikY = brill.gateL[1];
	
	var rPikX = brill.gateR[0];
	var rPikY = brill.gateR[1];
	
	var tPikX = brill.gateT[0];
	var tPikY = brill.gateT[1];
	
			//�������� � ������� ����� �����
			if(Math.abs(clickX-tPikX) <= 10 && Math.abs(clickY-tPikY) <= 10)
			{
				//����� �������� �������� �� ������� �����
				if(lordExist == false) return;
				
				//���������� ����� � ������� �����
				else{
				curVassal = new Target("brill", i, "top");
				
				for(var j = 0; j < connections.length; j++)
				{
					if(connections[j].lord.type == curVassal.type && connections[j].lord.id == curVassal.id && connections[j].lord.side == curVassal.side) return;
					if(connections[j].vassal.type == curVassal.type && connections[j].vassal.id == curVassal.id && connections[j].vassal.side == curVassal.side) return;
				}

				
				//�������� � ��������� ��������, ��� � ��� ���� �����
				if(curLord.type == "brill")
				brillMass[curLord.id].isConnected = true;
			
				else if (curLord.type == "rec")
				recMass[curLord.id].isConnected = true;
			
				else if (curLord.type == "ell")
				ellMass[curLord.id].isConnected = true;
			
				else if (curLord.type == "cicl")
				ciclMass[curLord.id].isConnected = true;
				
				brillMass[i].isConnected = true;

				
				//����������� ������� ������ � ��������� ��������
				if(curLord.type == "brill")
				brillMass[curLord.id].connections ++;
			
				else if (curLord.type == "rec")
				recMass[curLord.id].connections ++;

				else if (curLord.type == "ell")
				ellMass[curLord.id].connections ++;

				else if (curLord.type == "cicl")
				ciclMass[curLord.id].connections ++;			

				brillMass[i].connections ++;
				
				lordExist = false;
				circleExist = false;
				
				makeConnection(curLord, curVassal);
				return;
					
				}
			}
		
			//�������� � ����� ����� �����
			if(Math.abs(clickX-lPikX) <= 10 && Math.abs(clickY-lPikY) <= 10)
			{
				if(lordExist == false)
				{
				curLord = new Target("brill", i, "left");
				
				for(var j = 0; j < connections.length; j++)
				{
					if(connections[j].lord.type == curLord.type && connections[j].lord.id == curLord.id && connections[j].lord.side == curLord.side) return;
					if(connections[j].vassal.type == curLord.type && connections[j].vassal.id == curLord.id && connections[j].vassal.side == curLord.side) return;
				}
				lordExist = true;
				previousSelectedCircle.isSelected = false;	
				addCircle(lPikX,lPikY);
				
				//���������� �����, � ����� ������ ������������ ����� ������� ������ ����
				for(var z = 0; z<brillMass.length; z++)
				{
					//����� �� ����� � �������� ��� ��� ������
					if(brillMass[z].isConnected == false && z != i) drawHelpCircle(brillMass[z].gateT[0],brillMass[z].gateT[1]);

					//����� �� ����� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ���� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "brill") isVassal = true;
						}

						if(isVassal == false && z != i) drawHelpCircle(brillMass[z].gateT[0],brillMass[z].gateT[1]);
					}
				}
				
				//���������� �����, � ����� ��������������� ������������ ����� ������� ������ ����
				for(var z = 0; z<recMass.length; z++)
				{
					//����� �� �������������� � �������� ��� ��� ������
					if(recMass[z].isConnected == false) drawHelpCircle(recMass[z].gateT[0],recMass[z].gateT[1]);
					
					//����� �� �������������� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ���� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "rec") isVassal = true;
						}

						if(isVassal == false) drawHelpCircle(recMass[z].gateT[0],recMass[z].gateT[1]);
					}
				}
				
				//���������� �����, � ����� ������ ������������ ����� ������� ������ ����
				for(var z = 0; z<ellMass.length; z++)
				{
					//����� �� ���� � �������� ��� ��� ������
					if(ellMass[z].isConnected == false) {
						drawHelpCircle(ellMass[z].gateT[0],ellMass[z].gateT[1]);
					}
					
				}
				
				//���������� �����, � ����� �������� ����� ������������ ����� ������� ������ ����
				for(var z = 0; z<ciclMass.length; z++)
				{
					//����� �� ������� ����� � �������� ��� ��� ������
					if(ciclMass[z].isConnected == false && z != i) {
						drawHelpCircle(ciclMass[z].gateT[0],ciclMass[z].gateT[1]);
					}
					
					//����� �� ������� ����� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ������� ����� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "cicl") isVassal = true;
						}

						if(isVassal == false) drawHelpCircle(ciclMass[z].gateT[0],ciclMass[z].gateT[1]);
					}
				}
			
				
				return;
				}
				
				else return;
			}
			
			//�������� � ������ ����� �����
			else if(Math.abs(clickX-rPikX) <= 10 && Math.abs(clickY-rPikY) <= 10)
			{
				if(lordExist == false)
				{
				curLord = new Target("brill", i, "right");
				
				for(var j = 0; j < connections.length; j++)
				{
					if(connections[j].lord.type == curLord.type && connections[j].lord.id == curLord.id && connections[j].lord.side == curLord.side) return;
					if(connections[j].vassal.type == curLord.type && connections[j].vassal.id == curLord.id && connections[j].vassal.side == curLord.side) return;
				}
				lordExist = true;
				previousSelectedCircle.isSelected = false;
				
				addCircle(rPikX,rPikY);

				//���������� �����, � ����� ������ ������������ ����� ������� ������ ����
				for(var z = 0; z<brillMass.length; z++)
				{
					//����� �� ����� � �������� ��� ��� ������
					if(brillMass[z].isConnected == false && z != i) drawHelpCircle(brillMass[z].gateT[0],brillMass[z].gateT[1]);

					//����� �� ����� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ���� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "brill") isVassal = true;
						}

						if(isVassal == false && z != i) drawHelpCircle(brillMass[z].gateT[0],brillMass[z].gateT[1]);
					}
				}
				
				//���������� �����, � ����� ��������������� ������������ ����� ������� ������ ����
				for(var z = 0; z<recMass.length; z++)
				{
					//����� �� �������������� � �������� ��� ��� ������
					if(recMass[z].isConnected == false) drawHelpCircle(recMass[z].gateT[0],recMass[z].gateT[1]);
					
					//����� �� �������������� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ���� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "rec") isVassal = true;
						}

						if(isVassal == false) drawHelpCircle(recMass[z].gateT[0],recMass[z].gateT[1]);
					}
				}
				
				//���������� �����, � ����� ������ ������������ ����� ������� ������ ����
				for(var z = 0; z<ellMass.length; z++)
				{
					//����� �� ���� � �������� ��� ��� ������
					if(ellMass[z].isConnected == false) {
						drawHelpCircle(ellMass[z].gateT[0],ellMass[z].gateT[1]);
					}
					
				}
				
				//���������� �����, � ����� �������� ����� ������������ ����� ������� ������ ����
				for(var z = 0; z<ciclMass.length; z++)
				{
					//����� �� ������� ����� � �������� ��� ��� ������
					if(ciclMass[z].isConnected == false) {
						drawHelpCircle(ciclMass[z].gateT[0],ciclMass[z].gateT[1]);
					}
					
					//����� �� ������� ����� � �������� ��� ���� �����
					else{
						var isVassal = false;
						
						for(var n = 0; n < connections.length; n++)
						{
							//�������� �������� �� ������� ����� ���� ���� ��������
							if(connections[n].vassal.id == z && connections[n].vassal.type == "cicl") isVassal = true;
						}

						if(isVassal == false) drawHelpCircle(ciclMass[z].gateT[0],ciclMass[z].gateT[1]);
					}
				}
				
				return;
				}
				
				else return;
			}
	
	// ����������, ��������� �� �����, � ������� ��������, � ������ �����
    else if ((Math.abs((clickX-cX)/brill.width)+Math.abs((clickY-cY)/brill.height))<=0.5) 
	{
	
			if(circleExist == true) circleExist = false;
			if(lordExist == true) lordExist = false;

			// ���������� ���������� ��������� ����	
			if (previousSelectedCircle != null) previousSelectedCircle.isSelected = false;
			previousSelectedCircle = brill;

			// ������������� ����� ��������� ���� � ��������� �����
			brill.isSelected = true;
			drawAll();
	  
			isDragging = true;
			
			previousSelectedCircle.startDrX = clickX - previousSelectedCircle.x;
			previousSelectedCircle.startDrY = clickY - previousSelectedCircle.y;
			// ���������� ��������
			return;

    }
  }
	

	if(circleExist == true) circleExist = false;
	if(lordExist == true) lordExist = false;
    if (previousSelectedCircle != null) previousSelectedCircle.isSelected = false;
	drawAll();	

}

//����������� ��������������
 function stopDragging() {
  isDragging = false;
}

//�������������� �����
function dragCircle(e) {
  // �������� ����������� ��������������
  if (isDragging == true) {
    // �������� ���������
    if (previousSelectedCircle != null) {
      // ��������� ������� ����
      var x = e.pageX - canvas.offsetLeft;
      var y = e.pageY - canvas.offsetTop;

      // ���������� ������� � ����� �������
	    
      previousSelectedCircle.x = x - previousSelectedCircle.startDrX;
      previousSelectedCircle.y = y - previousSelectedCircle.startDrY;
	  
	  if(previousSelectedCircle.type == "brill")
		previousSelectedCircle.gates = chBrillGates(previousSelectedCircle);

	  if(previousSelectedCircle.type == "rec")
		previousSelectedCircle.gates = chRectGates(previousSelectedCircle);
	
	  if(previousSelectedCircle.type == "ell")
	  {
		 previousSelectedCircle.gates = chEllGates(previousSelectedCircle).splice(2,4);
		 previousSelectedCircle.centL = chEllGates(previousSelectedCircle)[0];
		 previousSelectedCircle.centR = chEllGates(previousSelectedCircle)[1];
		 
	  }
		
	  if(previousSelectedCircle.type == "cicl")
		previousSelectedCircle.gates = chCiclGates(previousSelectedCircle);
	
	  if(previousSelectedCircle.isConnected == true)
		reConnection(previousSelectedCircle);


      // ��������� �����
      drawAll();
    }
  }
}

//**************************************�������� ������ ����� ���������********

//������� ������ ����� �����/������
function findGate(target){
	var gates = [];
	
	if(target.type == "rec")
	{
		if(target.side == "left") gates = recMass[target.id].gateL;
		else if(target.side == "right") gates = recMass[target.id].gateR;
		else if(target.side == "top") gates = recMass[target.id].gateT;
		else if(target.side == "down") gates = recMass[target.id].gateD;
	}
	
	else if(target.type == "brill")
	{
		if(target.side == "left") gates = brillMass[target.id].gateL;
		else if(target.side == "right") gates = brillMass[target.id].gateR;
		else if(target.side == "top") gates = brillMass[target.id].gateT;
		else if(target.side == "down") gates = brillMass[target.id].gateD;
	}
	
	else if(target.type == "ell")
	{
		if(target.side == "left") gates = ellMass[target.id].gateL;
		else if(target.side == "right") gates = ellMass[target.id].gateR;
		else if(target.side == "top") gates = ellMass[target.id].gateT;
		else if(target.side == "down") gates = ellMass[target.id].gateD;
	}

	else if(target.type == "cicl")
	{
		if(target.side == "left") gates = ciclMass[target.id].gateL;
		else if(target.side == "right") gates = ciclMass[target.id].gateR;
		else if(target.side == "top") gates = ciclMass[target.id].gateT;
		else if(target.side == "down") gates = ciclMass[target.id].gateD;
	}		
	
	return gates;
}

//������� �������� Connection
function makeConnection(lord, vassal){
	
		var segment;
		var connection = new Connection(lord, vassal);
		
		var lordGate = findGate(lord);
		var vassalGate = findGate(vassal);
		
		var lordPoint = new Point(0,0);
		var vassalPoint = new Point(0,0);
		
		//��������� ����� �� ����� Lord`�
		if(lord.side == "left"){
			segment = new Segment(lordGate[0],lordGate[1],lordGate[0]-20,lordGate[1])
			connection.segments.push(segment);
			
			lordPoint.x = lordGate[0]-20;
			lordPoint.y = lordGate[1];
		}
		
		else if(lord.side == "right"){
			segment = new Segment(lordGate[0],lordGate[1],lordGate[0]+20,lordGate[1])
			connection.segments.push(segment);
			
			lordPoint.x = lordGate[0]+20;
			lordPoint.y = lordGate[1];
		}
		
		else if(lord.side == "top"){
			segment = new Segment(lordGate[0],lordGate[1],lordGate[0],lordGate[1]-20)
			connection.segments.push(segment);
			
			lordPoint.x = lordGate[0];
			lordPoint.y = lordGate[1]-20;
		}
		
		else if(lord.side == "down"){
			segment = new Segment(lordGate[0],lordGate[1],lordGate[0],lordGate[1]+20)
			connection.segments.push(segment);
			
			lordPoint.x = lordGate[0];
			lordPoint.y = lordGate[1]+20;
		}
		
		//��������� ����� �� ����� Vassal`�
		if(vassal.side == "left"){
			segment = new Segment(vassalGate[0],vassalGate[1],vassalGate[0]-20,vassalGate[1])
			connection.segments.push(segment);
			
			vassalPoint.x = vassalGate[0]-20;
			vassalPoint.y = vassalGate[1];
		}
		
		else if(vassal.side == "right"){
			segment = new Segment(vassalGate[0],vassalGate[1],vassalGate[0]+20,vassalGate[1])
			connection.segments.push(segment);
			
			vassalPoint.x = vassalGate[0]+20;
			vassalPoint.y = vassalGate[1];
		}
		
		else if(vassal.side == "top"){
			segment = new Segment(vassalGate[0],vassalGate[1],vassalGate[0],vassalGate[1]-20)
			connection.segments.push(segment);
			
			vassalPoint.x = vassalGate[0];
			vassalPoint.y = vassalGate[1]-20;
		}
		
		else if(vassal.side == "down"){
			segment = new Segment(vassalGate[0],vassalGate[1],vassalGate[0],vassalGate[1]+20)
			connection.segments.push(segment);
			
			vassalPoint.x = vassalGate[0];
			vassalPoint.y = vassalGate[1]+20;
		}
		
		//""������������"
		if(lordPoint.y > vassalPoint.y)
		{
			segment = new Segment(lordPoint.x,lordPoint.y,lordPoint.x,vassalPoint.y);
			
			connection.segments.push(segment);
			
			lordPoint.y = vassalPoint.y;
		}
		
		segment = new Segment(lordPoint.x,lordPoint.y,vassalPoint.x,vassalPoint.y);
		
		connection.segments.push(segment);
		
		connections.push(connection);
		
		drawAll();

		}
		
//��������� ��������� ����� �����
function chBrillGates(object){
	object.gateL = [object.x, object.y];
	object.gateR = [object.x+object.width, object.y];
	object.gateT = [object.x+object.width/2, object.y - object.height/2];
	object.gateD = [object.x+object.width/2, object.y + object.height/2];
	
	object.gates = [object.gateL, object.gateR, object.gateT, object.gateD]
	
	return object.gates;
}

//��������� ��������� ����� ��������������
function chRectGates(object){
	object.gateT = [object.x+object.width/2, object.y];
	object.gateD = [object.x+object.width/2, object.y + object.height];
	
	object.gates = [0,0, object.gateT, object.gateD];
	
	return object.gates;
}

//��������� ��������� ����� ������ �����
function chCiclGates(object){
	if(object.cType==0)
	{
		object.gateT = [object.x+object.dWidth/2, object.y - object.height];
		object.gateD = [object.x+object.dWidth/2, object.y];
	}
	
	else{
		object.gateT = [object.x+object.tWidth/2, object.y];
		object.gateD = [object.x+object.tWidth/2, object.y + object.height];
	}
	
	object.gates = [0,0, object.gateT, object.gateD];
	
	return object.gates;
}

//��������� ��������� ����� �����
function chEllGates(object){
	object.gateT = [object.x+(object.width-object.height)/2, object.y];
	object.gateD = [object.x+(object.width-object.height)/2, object.y + object.height];
	
	//����� ����� ����������
	object.centL = [object.x, object.y + object.height/2];
	object.centR = [object.x+(object.width-object.height), object.y + object.height/2];
	
	object.gates = [object.centL, object.centR, 0, 0, object.gateT, object.gateD];
	
	return object.gates;
}

//������� �������������� ������
function reConnection(object){

		for(var i = 0; i < object.connections; i++)
		{

			for(var j = 0; j < connections.length; j++)
			{
				
				if(connections[j].lord.type == object.type && connections[j].lord.id == object.id || connections[j].vassal.type == object.type && connections[j].vassal.id == object.id)
				{

					makeConnection(connections[j].lord, connections[j].vassal);
					connections.splice(j,1);
														
				}
			}
		}
}

//**************************************���������� ������ � �������***********
//������� ���������� ������ � �������
function setText(){
	if(previousSelectedCircle != null)
	{
	var newtext = prompt("������� ���������� �����");
	previousSelectedCircle.text = newtext;
	drawAll();
	}
	
	else alert("������� �������� �������!");
}

//**************************************�������������� �������************************************
function tetra(x1,y1,x2,y2,x3,y3){
	var S = (x1-x2)*(y3-y2)-(y1-y2)*(x3-x2);
	return S;
}

